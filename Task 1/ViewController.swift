//
//  ViewController.swift
//  Task 1
//
//  Created by Ranjith on 28/09/2021.
//

import UIKit

class ViewController: UIViewController {

    
    //Swift Basics
    // 1) Declaring Variables
    // let used for Non muttable Variables
    // var used for muttable Variables
    let name = "Ranjith Kumar" // String
    var Roll_no = 43   // Integer
    
   // 2) Data types in Swift
    
    
    var name1 = "Ranjith" // String
    var num0 = 12 // integer
    var num1 = 31.03 // FLoat
    var num2 = 12.567888 // Double
    let MyNameisRanjith = true // Boolean
    let MyAgeis45 = false // Boolean

    let AgeName = (21, "Ranjith") //Tuples - Tuples group multiple values into a single compound value. The values within a tuple can be of any type and don’t have to be of the same type as each other.
    
   let players = ["Dhoni","Virat","Raina","Rohit"]// Array - An array stores values of the same type in an ordered list. The same value can appear in an array multiple times at different positions.
    
    var favoriteGenres: Set = ["Rock", "Classical", "Hip hop"] // Set- A set stores distinct values of the same type in a collection with no defined ordering.
    
    var persons: [Int: String] = [21: "Ranjith", 26: "Kumar"] // Dictionary - It is an unordered collection of key Value Pairs
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        print("Hii \(name), your Roll no is \(Roll_no)" )
        
        // 3) loops
        //for loop
        // program to display numbers from 1 to 10
        for i in 1...10{
            print(i)
        }
        
        // while loop
        // program to display numbers from 1 to 5

        // initialize the variable
        var i = 1, n = 5
        while (i <= n) {
          print(i)
           i = i + 1
        }
        
        //4) If else
        
        
        let num = 40
        let num2 = 30
        if num >= num2 {
            print("num is greater than num2")
        } else {
            print("num is less than num2")
        }
        //5) Switch

        let someCharacter: Character = "z"
        switch someCharacter {
        case "a":
            print("The first letter of the alphabet")
        case "z":
            print("The last letter of the alphabet")
        default:
            print("Some other character")
        }
        //Calling the Function
        sum(a: 20, b: 10)
        sum(a: 40, b: 10)
        sum(a: 24, b: 6)
        // Do any additional setup after loading the view.
    }
    //Functions
    
    func sum(a: Int, b: Int) {
       let a = a + b
       let b = a - b
       print(a, b)
    }
}

